#include <OmniCameraGeometry.hpp>
#include <eigen3/Eigen/Dense>

namespace aslam {
namespace cameras {

OmniCameraGeometry::OmniCameraGeometry(double xi, double k1, double k2,
                                       double p1, double p2, double gamma1,
                                       double gamma2, double u0, double v0,
                                       int width, int height)
    : _xi(xi),
      _k1(k1),
      _k2(k2),
      _p1(p1),
      _p2(p2),
      _gamma1(gamma1),
      _gamma2(gamma2),
      _u0(u0),
      _v0(v0),
      _width(width),
      _height(height) {
  updateTemporaries();
}

OmniCameraGeometry::~OmniCameraGeometry() {

}

// This updates the intrinsic parameters with a small step: i <-- i + di
// The Jacobians above are with respect to this update function.
void OmniCameraGeometry::updateIntrinsicsOplus(double * di) {
  _xi += di[0];
  _k1 += di[1];
  _k2 += di[2];
  _p1 += di[3];
  _p2 += di[4];
  _gamma1 += di[5];
  _gamma2 += di[6];
  _u0 += di[7];
  _v0 += di[8];
  updateTemporaries();
}


/** 
 * \brief Lifts a point from the image plane to the unit sphere
 *
 * \param u u image coordinate
 * \param v v image coordinate
 * \param X X coordinate of the point on the sphere
 * \param Y Y coordinate of the point on the sphere
 * \param Z Z coordinate of the point on the sphere
 */
void OmniCameraGeometry::lift_sphere(double u, double v, double *X, double *Y,
                                     double *Z) const {
  double mx_d, my_d, mx_u, my_u;
  double lambda;

  // Lift points to normalised plane
  // Matlab points start at 1 (calibration)
  mx_d = _inv_K11 * (u) + _inv_K13;
  my_d = _inv_K22 * (v) + _inv_K23;

  // Recursive distortion model
  // int n = 6;
  // double dx_u, dy_u;
  // distortion(mx_d,my_d,&dx_u,&dy_u);
  // // Approximate value
  // mx_u = mx_d-dx_u;
  // my_u = my_d-dy_u;

  // for(int i=1;i<n;i++) {
  // 	distortion(mx_u,my_u,&dx_u,&dy_u);
  // 	mx_u = mx_d-dx_u;
  // 	my_u = my_d-dy_u;
  // }
  // PTF: March 31, 2012. The above was not that accurate.
  //      Substitute Gauss-Newton.
  undistortGN(mx_d, my_d, &mx_u, &my_u);

  // Lift normalised points to the sphere (inv_hslash)
  lambda = (_xi + sqrt(1 + (1 - _xi * _xi) * (mx_u * mx_u + my_u * my_u)))
      / (1 + mx_u * mx_u + my_u * my_u);
  *X = lambda * mx_u;
  *Y = lambda * my_u;
  *Z = lambda - _xi;
}

/** 
 * \brief Lifts a point from the image plane to its projective ray
 *
 * \param u u image coordinate
 * \param v v image coordinate
 * \param X X coordinate of the projective ray
 * \param Y Y coordinate of the projective ray
 * \param Z Z coordinate of the projective ray
 */
void OmniCameraGeometry::lift_projective(double u, double v, double *X,
                                         double *Y, double *Z) const {
  double mx_d, my_d, mx_u, my_u;
  double rho2_d;

  // Lift points to normalised plane
  // Matlab points start at 1 (calibration)
  mx_d = _inv_K11 * (u) + _inv_K13;
  my_d = _inv_K22 * (v) + _inv_K23;

  // Recursive distortion model
  // int n = 8;
  // double dx_u, dy_u;
  // distortion(mx_d,my_d,&dx_u,&dy_u);
  // // Approximate value
  // mx_u = mx_d-dx_u;
  // my_u = my_d-dy_u;

  // for(int i=1;i<n;i++) {
  // 	distortion(mx_u,my_u,&dx_u,&dy_u);
  // 	mx_u = mx_d-dx_u;
  // 	my_u = my_d-dy_u;
  // }
  // PTF: March 31, 2012. The above was not that accurate.
  //      Substitute Gauss-Newton.
  undistortGN(mx_d, my_d, &mx_u, &my_u);

  //std::cout << "lift projective: u: " << mx_u << ", v: " << my_u << std::endl;

  // Obtain a projective ray
  // Reuse variable
  rho2_d = mx_u * mx_u + my_u * my_u;
  *X = mx_u;
  *Y = my_u;
  *Z = 1 - _xi * (rho2_d + 1) / (_xi + sqrt(1 + (1 - _xi * _xi) * rho2_d));

  //std::cout << "lift projective: p: " << *X << ", " << *Y << ", " << *Z << std::endl;
}

/** 
 * \brief Project a 3D points (\a x,\a y,\a z) to the image plane in (\a u,\a v)
 *
 * \param x 3D point x coordinate
 * \param y 3D point y coordinate
 * \param z 3D point z coordinate
 * \param u return value, contains the image point u coordinate
 * \param v return value, contains the image point v coordinate
 */
void OmniCameraGeometry::space2plane(double x, double y, double z, double *u,
                                     double *v) const {
  double mx_u, my_u, mx_d, my_d;

  // Project points to the normalised plane
  z = z + _xi * sqrt(x * x + y * y + z * z);
  mx_u = x / z;
  my_u = y / z;

  // Apply distortion
  double dx_u, dy_u;
  distortion(mx_u, my_u, &dx_u, &dy_u);
  mx_d = mx_u + dx_u;
  my_d = my_u + dy_u;

  // Apply generalised projection matrix
  // Matlab points start at 1
  *u = _gamma1 * mx_d + _u0;
  *v = _gamma2 * my_d + _v0;
}

/** 
 * \brief Project a 3D points (\a x,\a y,\a z) to the image plane in (\a u,\a v)
 *        and calculate jacobian
 *
 * \param x 3D point x coordinate
 * \param y 3D point y coordinate
 * \param z 3D point z coordinate
 * \param u return value, contains the image point u coordinate
 * \param v return value, contains the image point v coordinate
 */
void OmniCameraGeometry::space2plane(double x, double y, double z, double *u,
                                     double *v, double *dudx, double *dvdx,
                                     double *dudy, double *dvdy, double *dudz,
                                     double *dvdz) const {
  double mx_u, my_u, mx_d, my_d;
  double norm, inv_denom;
  double dxdmx, dydmx, dxdmy, dydmy;

  norm = sqrt(x * x + y * y + z * z);
  // Project points to the normalised plane
  inv_denom = 1 / (z + _xi * norm);
  mx_u = inv_denom * x;
  my_u = inv_denom * y;

  // Calculate jacobian
  inv_denom = inv_denom * inv_denom / norm;
  *dudx = inv_denom * (norm * z + _xi * (y * y + z * z));
  *dvdx = -inv_denom * _xi * x * y;
  *dudy = *dvdx;
  *dvdy = inv_denom * (norm * z + _xi * (x * x + z * z));
  inv_denom = inv_denom * (-_xi * z - norm);  // reuse variable
  *dudz = x * inv_denom;
  *dvdz = y * inv_denom;

  // Apply distortion
  double dx_u, dy_u;
  distortion(mx_u, my_u, &dx_u, &dy_u, &dxdmx, &dydmx, &dxdmy, &dydmy);
  mx_d = mx_u + dx_u;
  my_d = my_u + dy_u;

  // Make the product of the jacobians
  // and add projection matrix jacobian
  inv_denom = _gamma1 * (*dudx * dxdmx + *dvdx * dxdmy);  // reuse
  *dvdx = _gamma2 * (*dudx * dydmx + *dvdx * dydmy);
  *dudx = inv_denom;

  inv_denom = _gamma1 * (*dudy * dxdmx + *dvdy * dxdmy);  // reuse
  *dvdy = _gamma2 * (*dudy * dydmx + *dvdy * dydmy);
  *dudy = inv_denom;

  inv_denom = _gamma1 * (*dudz * dxdmx + *dvdz * dxdmy);  // reuse
  *dvdz = _gamma2 * (*dudz * dydmx + *dvdz * dydmy);
  *dudz = inv_denom;

  // Apply generalised projection matrix
  // Matlab points start at 1
  *u = _gamma1 * mx_d + _u0;
  *v = _gamma2 * my_d + _v0;
}

/** 
 * \brief Projects an undistorted 2D point (\a mx_u,\a my_u) to the image plane in (\a u,\a v)
 *
 * \param mx_u 2D point x coordinate
 * \param my_u 3D point y coordinate
 * \param u return value, contains the image point u coordinate
 * \param v return value, contains the image point v coordinate
 */
void OmniCameraGeometry::undist2plane(double mx_u, double my_u, double *u,
                                      double *v) const {
  double mx_d, my_d;

  // Apply distortion
  double dx_u, dy_u;
  distortion(mx_u, my_u, &dx_u, &dy_u);
  mx_d = mx_u + dx_u;
  my_d = my_u + dy_u;

  // Apply generalised projection matrix
  // Matlab points start at 1
  *u = _gamma1 * mx_d + _u0;
  *v = _gamma2 * my_d + _v0;
}

/** 
 * \brief Apply distortion to input point (from the normalised plane)
 *  
 * \param mx_u undistorted x coordinate of point on the normalised plane
 * \param my_u undistorted y coordinate of point on the normalised plane
 * \param dx return value, to obtain the distorted point : mx_d = mx_u+dx_u
 * \param dy return value, to obtain the distorted point : my_d = my_u+dy_u
 */
void OmniCameraGeometry::distortion(double mx_u, double my_u, double *dx_u,
                                    double *dy_u) const {
  double mx2_u, my2_u, mxy_u, rho2_u, rad_dist_u;

  mx2_u = mx_u * mx_u;
  my2_u = my_u * my_u;
  mxy_u = mx_u * my_u;
  rho2_u = mx2_u + my2_u;
  rad_dist_u = _k1 * rho2_u + _k2 * rho2_u * rho2_u;
  *dx_u = mx_u * rad_dist_u + 2 * _p1 * mxy_u + _p2 * (rho2_u + 2 * mx2_u);
  *dy_u = my_u * rad_dist_u + 2 * _p2 * mxy_u + _p1 * (rho2_u + 2 * my2_u);
}

/** 
 * \brief Apply distortion to input point (from the normalised plane)
 *        and calculate jacobian
 *
 * \param mx_u undistorted x coordinate of point on the normalised plane
 * \param my_u undistorted y coordinate of point on the normalised plane
 * \param dx return value, to obtain the distorted point : mx_d = mx_u+dx_u
 * \param dy return value, to obtain the distorted point : my_d = my_u+dy_u
 */
void OmniCameraGeometry::distortion(double mx_u, double my_u, double *dx_u,
                                    double *dy_u, double *dxdmx, double *dydmx,
                                    double *dxdmy, double *dydmy) const {
  double mx2_u, my2_u, mxy_u, rho2_u, rad_dist_u;

  mx2_u = mx_u * mx_u;
  my2_u = my_u * my_u;
  mxy_u = mx_u * my_u;
  rho2_u = mx2_u + my2_u;
  rad_dist_u = _k1 * rho2_u + _k2 * rho2_u * rho2_u;
  *dx_u = mx_u * rad_dist_u + 2 * _p1 * mxy_u + _p2 * (rho2_u + 2 * mx2_u);
  *dy_u = my_u * rad_dist_u + 2 * _p2 * mxy_u + _p1 * (rho2_u + 2 * my2_u);

  *dxdmx = 1 + rad_dist_u + _k1 * 2 * mx2_u + _k2 * rho2_u * 4 * mx2_u
      + 2 * _p1 * my_u + 6 * _p2 * mx_u;
  *dydmx = _k1 * 2 * mx_u * my_u + _k2 * 4 * rho2_u * mx_u * my_u
      + _p1 * 2 * mx_u + 2 * _p2 * my_u;
  *dxdmy = *dydmx;
  *dydmy = 1 + rad_dist_u + _k1 * 2 * my2_u + _k2 * rho2_u * 4 * my2_u
      + 6 * _p1 * my_u + 2 * _p2 * mx_u;
}

void OmniCameraGeometry::updateTemporaries() {
  // Inverse camera projection matrix parameters
  _inv_K11 = 1.0 / _gamma1;
  _inv_K13 = -_u0 / _gamma1;
  _inv_K22 = 1.0 / _gamma2;
  _inv_K23 = -_v0 / _gamma2;
  _one_over_xixi_m_1 = 1.0 / (_xi * _xi - 1.0);

}

void OmniCameraGeometry::setIntrinsicsVectorImplementation(
    const Eigen::VectorXd & V) {
  _xi = V[0];
  _k1 = V[1];
  _k2 = V[2];
  _p1 = V[3];
  _p2 = V[4];
  _gamma1 = V[5];
  _gamma2 = V[6];
  _u0 = V[7];
  _v0 = V[8];
  updateTemporaries();

}

Eigen::VectorXd OmniCameraGeometry::getIntrinsicsVectorImplementation() const {
  Eigen::VectorXd V(9);
  V[0] = _xi;
  V[1] = _k1;
  V[2] = _k2;
  V[3] = _p1;
  V[4] = _p2;
  V[5] = _gamma1;
  V[6] = _gamma2;
  V[7] = _u0;
  V[8] = _v0;
  return V;
}

Eigen::Matrix3d OmniCameraGeometry::getCameraMatrix() const {
  Eigen::Matrix3d K;
  K << _gamma1, 0, _u0, 0, _gamma2, _v0, 0, 0, 1;
  return K;
}

// Use Gauss-Newton to undistort.
void OmniCameraGeometry::undistortGN(double u_d, double v_d, double * u,
                                     double * v) const {
  *u = u_d;
  *v = v_d;

  double ubar = u_d;
  double vbar = v_d;
  const int n = 5;
  Eigen::Matrix2d F;

  double hat_u_d;
  double hat_v_d;

  // void OmniCameraGeometry::distortion(double mx_u, double my_u, 
  // 					  double *dx_u, double *dy_u,
  // 					  double *dxdmx, double *dydmx,
  // 					  double *dxdmy, double *dydmy) const
  for (int i = 0; i < n; i++) {
    distortion(ubar, vbar, &hat_u_d, &hat_v_d, &F(0, 0), &F(1, 0), &F(0, 1),
               &F(1, 1));

    Eigen::Vector2d e(u_d - ubar - hat_u_d, v_d - vbar - hat_v_d);
    Eigen::Vector2d du = (F.transpose() * F).inverse() * F.transpose() * e;

    ubar += du[0];
    vbar += du[1];

    if (e.dot(e) < 1e-15)
      break;

  }
  *u = ubar;
  *v = vbar;
}

    }  // namespace cameras  

    }  // namespace aslam
