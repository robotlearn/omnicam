#include <pybind11/pybind11.h>
#include <tuple>
#include "OmniCameraGeometry.hpp"

namespace py = pybind11;

using namespace aslam;
using namespace cameras;


std::tuple<double, double> py_undistortGN(OmniCameraGeometry& geom, double u_d, double v_d) {
    double u,v;
    geom.undistortGN(u_d, v_d, &u, &v);
    return std::tuple<double, double>(u, v);
}

std::tuple<double, double, double> py_lift_projective(OmniCameraGeometry& geom, double u_d, double v_d) {
    double x,y,z;
    geom.lift_projective(u_d, v_d, &x, &y, &z);
    return std::tuple<double, double, double>(x,y,z);
}

PYBIND11_MODULE(pybind_omnicam, m) {
    py::class_<OmniCameraGeometry>(m, "OmniCameraGeometry")
        .def(py::init<double, double, double, double, double, double, double, double, double, int, int>())
        .def("undistortGN", py_undistortGN)
        .def("lift_projective", py_lift_projective);
        ;
}