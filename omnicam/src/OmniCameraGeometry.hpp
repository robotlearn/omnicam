#ifndef OMNI_CAMERA8GEOMETRY_HPP
#define OMNI_CAMERA8GEOMETRY_HPP
#include <eigen3/Eigen/Dense>

struct
{
  int _x;
  int _y;
} keypoint_t;

namespace aslam
{
  namespace cameras
  {
    class OmniCameraGeometry
    {
    public:
      OmniCameraGeometry(double xi, double k1, double k2,
                         double p1, double p2, double gamma1,
                         double gamma2, double u0, double v0,
                         int width, int height);
      ~OmniCameraGeometry();
      void updateIntrinsicsOplus(double *di);
      void lift_projective(double u, double v, double *X,
                                         double *Y, double *Z) const;
      void lift_sphere(double u, double v, double *X, double *Y,
                       double *Z) const;
      void space2plane(double x, double y, double z, double *u,
                       double *v) const;

      void space2plane(double x, double y, double z, double *u,
                       double *v, double *dudx, double *dvdx,
                       double *dudy, double *dvdy, double *dudz,
                       double *dvdz) const;

      void undist2plane(double mx_u, double my_u, double *u,
                        double *v) const;

      void distortion(double mx_u, double my_u, double *dx_u,
                      double *dy_u) const;
      void distortion(double mx_u, double my_u, double *dx_u,
                      double *dy_u, double *dxdmx, double *dydmx,
                      double *dxdmy, double *dydmy) const;
 
      void updateTemporaries();

      void setIntrinsicsVectorImplementation(
          const Eigen::VectorXd &V);

          Eigen::VectorXd getIntrinsicsVectorImplementation() const;

          Eigen::Matrix3d getCameraMatrix() const;


void undistortGN(double u_d, double v_d, double * u,
                                     double * v) const;

    private:
      double _xi;
      double _k1;
      double _k2;
      double _p1;
      double _p2;
      double _gamma1;
      double _gamma2;
      double _u0;
      double _v0;
      int _width;
      int _height;

      double _inv_K11;
      double _inv_K13;
      double _inv_K22;
      double _inv_K23;
      double _one_over_xixi_m_1;
    };

  } // namespace cameras

} // namespace aslam
#endif