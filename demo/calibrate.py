import omnicam
import numpy as np
import cv2

geom = omnicam.OmniCameraGeometry(2.79,-0.09,0.5,-0.003,-0.002,1729.,1729.,637.,482.,1280,960)

#pts_2d = np.array([[495., 905.], [753., 909.], [538., 835.], [711., 837.]])
#pts_3d = np.array([[-60., 180.],[60., 180.],[-60., 300.],[60., 300.]])
# pts_2d = np.array([[441., 838.], [1039., 750.], [1052., 828.], [974., 706.]])  # in pixel
# pts_3d = np.array([[-1., 2.],[3., 2.],[2., 1.],[4., 3.95]])  # in meter, (x, y) -> (right, front)
pts_2d = np.array([[322., 750.], [642., 923.], [529., 689.], [916., 659.]])  # in pixel
pts_3d = np.array([[-2., 2.],[0., 1.],[-1., 3.95],[4., 3.95]])  # in meter, (x, y) -> (right, front)

pts_uv = np.zeros_like(pts_2d)
for i, pt in enumerate(pts_2d):
    x,y,z = geom.lift_projective(pt[0], pt[1])
    pts_uv[i, 0] = x/z
    pts_uv[i, 1] = y/z

H = cv2.findHomography(pts_uv, pts_3d)
print('H', H)